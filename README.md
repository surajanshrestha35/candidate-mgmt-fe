# IMP Message 💡: Why I chose Gitlab over Github?

Although my goto service is **Github,** I chose **Gitlab** because my current company monitors my activities on Github and I don't want them to know that I'm applying to other companies.

I'm super active on **Github** and you can check out my contributions there.
Thanks for understanding.

My Github: [https://github.com/SurajanShrestha/](https://github.com/SurajanShrestha/)

# How to run?

1. Make sure you have Docker and Docker Compose installed.
   I recommend installing [Docker Desktop](https://www.docker.com/products/docker-desktop/) as it comes pre-installed with docker engine, docker, docker compose and any other tools needed.
2. Open Docker Desktop
3. Clone this project
4. In root directory, run: `docker compose up --build`
5. Run Backend Server:
   1. Clone this repo and follow the instructions in the README: [https://gitlab.com/surajanshrestha35/candidate-mgmt](https://gitlab.com/surajanshrestha35/candidate-mgmt)

---

## Tech Used

1. Language: Typescript
2. Frontend: React
3. Bundler: Vite
4. UI Library: Mantine UI
5. Routing: React Router
