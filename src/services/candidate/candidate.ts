import API from "../api";
import {
  ICandidateCreateResponse,
  ICandidatePayload,
  ICandidateResponse,
} from "./types";

export const createOrUpdateCandidate = async (payload: ICandidatePayload) => {
  return API.post<ICandidateCreateResponse>("/candidates", payload);
};

export const getAllCandidates = async (): Promise<ICandidateResponse[]> => {
  const candidates = await API.get<ICandidateResponse[]>("/candidates");
  return candidates?.data;
};
