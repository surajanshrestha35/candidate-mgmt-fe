export interface ICandidatePayload {
  firstName: string;
  lastName: string;
  phoneNumber?: string;
  email: string;
  callTimeStart?: string;
  callTimeEnd?: string;
  linkedInUrl?: string;
  githubUrl?: string;
  comment: string;
}

export interface ICandidateCreateResponse {
  message: string;
  data: ICandidateResponse;
}

export interface ICandidateResponse extends ICandidatePayload {
  id: string;
  createdAt: string;
  updatedAt: string;
}
