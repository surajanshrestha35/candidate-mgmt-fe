import axios from "axios";

const API_URL = import.meta.env.VITE_API_URL;

const API = axios.create({
  baseURL: API_URL,
  headers: {
    "Content-Type": "application/json",
  },
});

// Note 👉: Can use interceptors here (mostly for attaching authorization headers, etc.)
// API.interceptors.request.use(async (config) => {
//   // Interceptor Logic
// });

export default API;
