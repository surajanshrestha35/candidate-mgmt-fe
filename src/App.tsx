import { Route, Routes } from "react-router-dom";
import "./App.css";
import { AddCandidate, ViewCandidates } from "./components/views";

function App() {
  return (
    <Routes>
      <Route path="/" element={<AddCandidate />} />
      <Route path="/view" element={<ViewCandidates />} />
    </Routes>
  );
}

export default App;
