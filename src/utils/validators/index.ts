export const validateText = (value: string) =>
  !value.length ? "Required" : null;

export const validatePhone = (value?: string) => {
  const regex = /^[+]?[\d\s()-]+$/;
  return !value ? null : !regex.test(value) ? "Invalid phone number" : null;
};

export const validateEmail = (value: string) => {
  const emailRegex = /^[\w.%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/;
  return !emailRegex.test(value) ? "Invalid email" : null;
};
