import { CandidateForm } from "../../../organisms";

export const AddCandidate = () => {
  return (
    <div>
      <h2>Add/Update Candidate</h2>
      <CandidateForm />
    </div>
  );
};
