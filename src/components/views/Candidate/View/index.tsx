import { Button, Paper } from "@mantine/core";
import { useQuery } from "@tanstack/react-query";
import { getAllCandidates } from "../../../../services";
import { useNavigate } from "react-router-dom";
import { IconArrowLeft } from "@tabler/icons-react";
import { CandidatesTable } from "../../../organisms";

export const ViewCandidates = () => {
  const navigate = useNavigate();

  const { data, isFetching } = useQuery({
    queryKey: ["candidates"],
    queryFn: () => getAllCandidates(),
  });

  const goBack = () => {
    navigate("/");
  };

  return (
    <div>
      <Paper
        shadow="md"
        px="md"
        mah={"75vh"}
        withBorder
        style={{ overflow: "auto" }}
      >
        <h2>View All Candidates</h2>
        <CandidatesTable isLoading={isFetching} data={data} />
      </Paper>
      <Button
        variant="outline"
        onClick={goBack}
        leftSection={<IconArrowLeft size={16} />}
        mt={12}
      >
        Back
      </Button>
    </div>
  );
};
