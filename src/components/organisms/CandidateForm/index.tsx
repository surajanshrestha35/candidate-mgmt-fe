import { Box, Button, Flex, Grid, Input, Textarea } from "@mantine/core";
import { TimeInput } from "@mantine/dates";
import {
  IconAt,
  IconEye,
  IconLink,
  IconPhone,
  IconSend,
  IconUser,
} from "@tabler/icons-react";
import { useForm } from "@mantine/form";
import { useMutation } from "@tanstack/react-query";
import { notifications } from "@mantine/notifications";
import { useNavigate } from "react-router-dom";
import { InputField } from "../../atoms";
import { validateEmail, validatePhone, validateText } from "../../../utils";
import { createOrUpdateCandidate, ICandidatePayload } from "../../../services";

export const CandidateForm = () => {
  const navigate = useNavigate();

  const form = useForm<ICandidatePayload>({
    mode: "uncontrolled",
    initialValues: {
      firstName: "",
      lastName: "",
      email: "",
      comment: "",
      phoneNumber: "",
      linkedInUrl: "",
      githubUrl: "",
      callTimeStart: "",
      callTimeEnd: "",
    },
    validate: {
      firstName: (value) => validateText(value),
      lastName: (value) => validateText(value),
      email: (value) => validateEmail(value),
      comment: (value) => validateText(value),
      phoneNumber: (value) => validatePhone(value),
    },
  });

  const { mutate, isPending } = useMutation({
    mutationFn: createOrUpdateCandidate,
    onSuccess: (data) => {
      notifications.show({
        title: "Success",
        message: data?.data?.message,
        color: "green",
      });
    },
    onError: () => {
      notifications.show({
        title: "Oops",
        message: "Something went wrong",
        color: "red",
      });
    },
  });

  const gotoView = () => {
    navigate("/view");
  };

  return (
    <form onSubmit={form.onSubmit((values) => mutate(values))}>
      <Grid maw={800}>
        <Grid.Col span={{ base: 12, md: 6 }}>
          <InputField
            label="First Name"
            required={true}
            placeholder="Enter First Name"
            leftSection={<IconUser size={16} />}
            maxLength={50}
            {...form.getInputProps("firstName")}
          />
        </Grid.Col>
        <Grid.Col span={{ base: 12, md: 6 }}>
          <InputField
            label="Last Name"
            required={true}
            placeholder="Enter Last Name"
            leftSection={<IconUser size={16} />}
            maxLength={50}
            {...form.getInputProps("lastName")}
          />
        </Grid.Col>
        <Grid.Col span={{ base: 12, md: 6 }}>
          <InputField
            label="Phone Number"
            placeholder="Enter Phone Number"
            leftSection={<IconPhone size={16} />}
            maxLength={15}
            {...form.getInputProps("phoneNumber")}
          />
        </Grid.Col>
        <Grid.Col span={{ base: 12, md: 6 }}>
          <InputField
            label="Email"
            required={true}
            placeholder="Enter Email"
            leftSection={<IconAt size={16} />}
            minLength={5}
            maxLength={100}
            {...form.getInputProps("email")}
          />
        </Grid.Col>
        <Grid.Col span={{ base: 12, md: 6 }}>
          <InputField
            label="LinkedIn Profile URL"
            placeholder="Enter LinkedIn Profile URL"
            leftSection={<IconLink size={16} />}
            maxLength={255}
            {...form.getInputProps("linkedInUrl")}
          />
        </Grid.Col>
        <Grid.Col span={{ base: 12, md: 6 }}>
          <InputField
            label="Github Profile URL"
            placeholder="Enter Github Profile URL"
            leftSection={<IconLink size={16} />}
            maxLength={255}
            {...form.getInputProps("githubUrl")}
          />
        </Grid.Col>
        <Grid.Col span={{ base: 12, md: 6 }}>
          <Input.Label>
            Time Interval in which we can contact the candidate
          </Input.Label>
          <Grid>
            <Grid.Col span={6}>
              <Box>
                <TimeInput
                  description="Start Time"
                  placeholder="Enter Start Time"
                  onClick={(e) => e?.currentTarget?.showPicker()}
                  {...form.getInputProps("callTimeStart")}
                />
              </Box>
            </Grid.Col>
            <Grid.Col span={6}>
              <Box>
                <TimeInput
                  description="End Time"
                  placeholder="Enter End Time"
                  onClick={(e) => e?.currentTarget?.showPicker()}
                  {...form.getInputProps("callTimeEnd")}
                />
              </Box>
            </Grid.Col>
          </Grid>
        </Grid.Col>
        <Grid.Col span={12}>
          <Box>
            <Textarea
              label="Comment"
              placeholder="Enter comments here"
              withAsterisk
              resize="vertical"
              rows={6}
              {...form.getInputProps("comment")}
            />
          </Box>
        </Grid.Col>
        <Flex px={8} gap={12}>
          <Button
            type="submit"
            loading={isPending}
            leftSection={<IconSend size={16} />}
          >
            Submit
          </Button>
          <Button
            variant="outline"
            onClick={gotoView}
            leftSection={<IconEye size={16} />}
          >
            View All Candidates
          </Button>
        </Flex>
      </Grid>
    </form>
  );
};
