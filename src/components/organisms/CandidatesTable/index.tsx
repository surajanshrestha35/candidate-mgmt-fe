import { Box, Loader, Table, Text } from "@mantine/core";
import { ICandidatesTableProps } from "./types";
import { useMemo } from "react";

export const CandidatesTable = ({ isLoading, data }: ICandidatesTableProps) => {
  const candidates = useMemo(() => {
    return data?.map((d) => ({
      id: d?.id,
      firstName: d?.firstName,
      lastName: d?.lastName,
      phoneNumber: d?.phoneNumber,
      email: d?.email,
      callTime: `${d?.callTimeStart}-${d?.callTimeEnd}`,
      linkedInUrl: d?.linkedInUrl,
      githubUrl: d?.githubUrl,
      comment: d?.comment ? d?.comment?.slice(0, 20) + "..." : "",
      createdAt: d?.createdAt?.slice(0, 10),
      updatedAt: d?.updatedAt?.slice(0, 10),
    }));
  }, [data]);

  const rows = useMemo(() => {
    return candidates?.map((d) => (
      <Table.Tr key={d?.id}>
        <Table.Td>{d?.id}</Table.Td>
        <Table.Td>{d?.firstName}</Table.Td>
        <Table.Td>{d?.lastName}</Table.Td>
        <Table.Td>{d?.phoneNumber}</Table.Td>
        <Table.Td>{d?.email}</Table.Td>
        <Table.Td>{d?.callTime}</Table.Td>
        <Table.Td>{d?.linkedInUrl}</Table.Td>
        <Table.Td>{d?.githubUrl}</Table.Td>
        <Table.Td>{d?.comment}</Table.Td>
        <Table.Td>{d?.createdAt}</Table.Td>
        <Table.Td>{d?.updatedAt}</Table.Td>
      </Table.Tr>
    ));
  }, [candidates]);

  return (
    <Table highlightOnHover mih={200}>
      <Table.Thead>
        <Table.Tr>
          <Table.Th>id</Table.Th>
          <Table.Th>First Name</Table.Th>
          <Table.Th>Last Name</Table.Th>
          <Table.Th>Phone Number</Table.Th>
          <Table.Th>Email</Table.Th>
          <Table.Th>Call Time</Table.Th>
          <Table.Th>LinkedIn URL</Table.Th>
          <Table.Th>Github URL</Table.Th>
          <Table.Th>Comment</Table.Th>
          <Table.Th>Created At</Table.Th>
          <Table.Th>Updated At</Table.Th>
        </Table.Tr>
      </Table.Thead>
      {isLoading ? (
        <Box pos={"absolute"} left={"48%"} top={"50%"}>
          <Loader />
        </Box>
      ) : rows?.length ? (
        <Table.Tbody>{rows}</Table.Tbody>
      ) : (
        <Box pos={"absolute"} left={"48%"} top={"50%"}>
          <Text>No Data</Text>
        </Box>
      )}
    </Table>
  );
};
