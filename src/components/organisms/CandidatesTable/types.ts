import { ICandidateResponse } from "../../../services";

export interface ICandidatesTableProps {
  isLoading: boolean;
  data?: ICandidateResponse[];
}
