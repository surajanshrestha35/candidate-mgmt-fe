import { TextInputProps } from "@mantine/core";

export interface IInputField extends TextInputProps {
  required?: boolean;
  placeholder?: string;
}
