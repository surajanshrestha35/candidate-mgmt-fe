import { Box, TextInput } from "@mantine/core";
import { IInputField } from "./types";

export const InputField = ({
  label,
  required,
  placeholder,
  ...rest
}: IInputField) => {
  return (
    <Box>
      <TextInput
        label={label}
        placeholder={placeholder}
        withAsterisk={required}
        {...rest}
      />
    </Box>
  );
};
